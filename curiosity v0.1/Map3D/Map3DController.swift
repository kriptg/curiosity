//
//  Map3DController.swift
//  curiosity v0.1
//
//  Created by Kiryl Mikhailovich on 4/4/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import UIKit
import MetalKit
import ModelIO
import ARKit

class Map3DController: UIViewController, Map3DViewProtocol, MTKViewDelegate {

    var mapPresenter: Map3DPresenterProtocol?
    var session: ARSession?

    private var map: Map?
    var mtkView: MTKView?
    var renderer: Renderer?
    var activityView: UIActivityIndicatorView?
    var editButton: UIBarButtonItem?
    var previousAnchor: ARAnchor?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        setActivityView()
        setToolBar()
        setMTKView()

        session = ARSession()
        mapPresenter?.attachView(self)
    }

    override func viewWillAppear(_ animated: Bool) {
        mtkView?.isHidden = true
        editButton?.isEnabled = true
        startLoading()

        guard let session = session else { return }
        configureSession(session: session)
    }

    override func viewWillDisappear(_ animated: Bool) {
        mtkView?.isPaused = true
        session?.pause()
    }

    override func viewDidAppear(_ animated: Bool) {
        guard let mtkView = mtkView else { return }
        guard let device = MTLCreateSystemDefaultDevice() else { return }
        guard let map = map else { return }

        guard let session = session else { return }
        if mtkView.isHidden {
            renderer = Renderer(session: session, view: mtkView, device: device, map: map)
        } else {
           renderer?.updateTexturesOnEdit()
        }
        renderer?.drawRectResized(size: mtkView.frame.size)

        mtkView.device = device

        stopLoading()
        mtkView.isHidden = false
        mtkView.isPaused = false
    }

    func startLoading() {
        activityView?.startAnimating()
    }

    func stopLoading() {

        activityView?.stopAnimating()
        activityView?.isHidden = true
    }

    func setMap(_ map: Map) {
        self.map = map
    }

    // MARK: Navigation Bar Items

    func setNavigationBar(_ title: String) {
        self.navigationItem.title = title
        editButton = UIBarButtonItem(title: "Edit",
                                     style: .plain,
                                     target: self,
                                     action: #selector(showEdit))

        guard let roverImage = UIImage(named: "rover") else { return }
        let routeButton = UIBarButtonItem(image: roverImage,
                                          style: .plain,
                                          target: self,
                                          action: #selector(animateRoute))

        guard let editButton = editButton else { return }
        self.navigationItem.rightBarButtonItems = [routeButton, editButton]
    }

    @objc func showEdit() {
        editButton?.isEnabled = false
        mapPresenter?.showEditView()
    }

    @objc func animateRoute() {
        mapPresenter?.findOptimalWay()
    }

    // MARK: Toolbar

    func setToolBar() {
        navigationController?.isToolbarHidden = false

        let clearButton = UIBarButtonItem(title: "Clear",
                                          style: .plain,
                                          target: self,
                                          action: #selector(refreshAnchors))

        self.toolbarItems = [clearButton]
    }

    @objc func refreshAnchors() {
        guard let session = session else { return }
        configureSession(session: session)
    }

    func configureSession(session: ARSession) {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal]
        configuration.isLightEstimationEnabled = true
        session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
    }

    // MARK: Activity and MTKView

    func setActivityView() {

        guard let toolbarHeight = self.navigationController?.toolbar.frame.size.height else { return }
        guard let navigationBarHeight = self.navigationController?.navigationBar.frame.size.height else { return }

        let window = UIApplication.shared.windows[0]
        let heightActivityView = self.view.bounds.height - window.safeAreaInsets.top - window.safeAreaInsets.bottom -
            toolbarHeight - navigationBarHeight

        let activityView = UIActivityIndicatorView(style: .gray)
        activityView.center = CGPoint (x: self.view.bounds.width / 2, y: heightActivityView / 2)
        self.activityView = activityView
        view.addSubview(activityView)
    }

    func setMTKView() {

        let mapViewRect = CGRect(x: 0,
                                 y: 0,
                                 width: self.view.bounds.width,
                                 height: self.view.bounds.height)

        mtkView = MTKView(frame: mapViewRect)
        guard let mtkView = mtkView else { return }
        mtkView.delegate = self

        mtkView.colorPixelFormat = .bgra8Unorm_srgb
        mtkView.depthStencilPixelFormat = .depth32Float_stencil8

        view.addSubview(mtkView)
    }

    func passOptimalWayToRouter(_ way: [[Int]]?) {
        renderer?.setDataToDrawWay(way)
    }

    func alertError(_ errorString: String, _ titleString: String) {
        let alert = UIAlertController(title: "\(errorString)", message: "\(titleString)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func showEditView(_ mapPresenter: MapPresenter) {
        let mapController = MapController()
        mapController.mapPresenter = mapPresenter
        navigationController?.pushViewController(mapController, animated: true)
    }

    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        renderer?.drawRectResized(size: size)
    }

    func draw(in view: MTKView) {
        renderer?.draw(in: view)
    }
}
