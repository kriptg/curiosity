//
//  MathUtilities.swift
//  curiosity v0.1
//
//  Created by Kiryl Mikhailovich on 4/4/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import simd

extension float4 {
    var xyz: float3 {
        return float3(x, y, z)
    }
}

extension float4x4 {
    init(scaleBy s: Float) {
        self.init(float4(s, 0, 0, 0),
                  float4(0, s, 0, 0),
                  float4(0, 0, s, 0),
                  float4(0, 0, 0, 1))
    }

    init(rotationAbout axis: float3, by angleRadians: Float) {
        let quat = simd_quatf(angle: angleRadians, axis: axis)
        self.init(quat)
    }

    init(translationBy t: float3) {
        self.init(float4(1, 0, 0, 0),
                  float4(0, 1, 0, 0),
                  float4(0, 0, 1, 0),
                  float4(t[0], t[1], t[2], 1))
    }

    init(perspectiveProjectionFov fovRadians: Float, aspectRatio aspect: Float, nearZ: Float, farZ: Float) {
        let yScale = 1 / tan(fovRadians * 0.5)
        let xScale = yScale / aspect
        let zRange = farZ - nearZ
        let zScale = -(farZ + nearZ) / zRange
        let wzScale = -2 * farZ * nearZ / zRange

        let xx = xScale
        let yy = yScale
        let zz = zScale
        let zw = Float(-1)
        let wz = wzScale

        self.init(float4(xx, 0, 0, 0),
                  float4(0, yy, 0, 0),
                  float4(0, 0, zz, zw),
                  float4(0, 0, wz, 0))
    }

    var normalMatrix: float3x3 {
        let upperLeft = float3x3(self[0].xyz, self[1].xyz, self[2].xyz)
        return upperLeft.transpose.inverse
    }
}
