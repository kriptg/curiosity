//
//  CapturedImageRenderer.swift
//  curiosity v0.1
//
//  Created by Kiryl Mikhailovich on 5/3/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import Foundation
import MetalKit

class CaptureImageSettingsRenderer: AbstractFactorySettingsRenderer {

    func buildVertexDescriptor() -> MDLVertexDescriptor? {
        let imagePlaneVertexDescriptor = MTLVertexDescriptor()

        imagePlaneVertexDescriptor.attributes[0].format = .float2
        imagePlaneVertexDescriptor.attributes[0].offset = 0
        imagePlaneVertexDescriptor.attributes[0].bufferIndex = 0

        imagePlaneVertexDescriptor.attributes[1].format = .float2
        imagePlaneVertexDescriptor.attributes[1].offset = MemoryLayout<float2>.stride
        imagePlaneVertexDescriptor.attributes[1].bufferIndex = 0

        imagePlaneVertexDescriptor.layouts[0].stride = MemoryLayout<float2>.stride * 2
        imagePlaneVertexDescriptor.layouts[0].stepRate = 1
        imagePlaneVertexDescriptor.layouts[0].stepFunction = .perVertex
        return MTKModelIOVertexDescriptorFromMetal(imagePlaneVertexDescriptor)
    }

    func buildSamplerState(device: MTLDevice) -> MTLSamplerState? {
        let samplerDescriptor = MTLSamplerDescriptor()
        samplerDescriptor.normalizedCoordinates = true
        samplerDescriptor.minFilter = .linear
        samplerDescriptor.magFilter = .linear
        samplerDescriptor.mipFilter = .linear
        return device.makeSamplerState(descriptor: samplerDescriptor)
    }

    func buildDepthStencilState(device: MTLDevice) -> MTLDepthStencilState? {
        let capturedImageDepthStateDescriptor = MTLDepthStencilDescriptor()
        capturedImageDepthStateDescriptor.depthCompareFunction = .always
        capturedImageDepthStateDescriptor.isDepthWriteEnabled = false
        return device.makeDepthStencilState(descriptor: capturedImageDepthStateDescriptor)
    }

    func buildPipeline(device: MTLDevice, view: MTKView, vertexDescriptor: MDLVertexDescriptor) -> MTLRenderPipelineState {
        guard let library = device.makeDefaultLibrary() else {
            fatalError("Could not load default library from main bundle")
        }

        let capturedImageVertexFunction = library.makeFunction(name: "capturedImageVertexTransform")
        let capturedImageFragmentFunction = library.makeFunction(name: "capturedImageFragmentShader")

        let capturedImagePipelineStateDescriptor = MTLRenderPipelineDescriptor()
        capturedImagePipelineStateDescriptor.label = "MyCapturedImagePipeline"
        capturedImagePipelineStateDescriptor.sampleCount = 1
        capturedImagePipelineStateDescriptor.vertexFunction = capturedImageVertexFunction
        capturedImagePipelineStateDescriptor.fragmentFunction = capturedImageFragmentFunction

        let mtlVertexDescriptor = MTKMetalVertexDescriptorFromModelIO(vertexDescriptor)
        capturedImagePipelineStateDescriptor.vertexDescriptor = mtlVertexDescriptor

        capturedImagePipelineStateDescriptor.colorAttachments[0].pixelFormat = view.colorPixelFormat
        capturedImagePipelineStateDescriptor.depthAttachmentPixelFormat = view.depthStencilPixelFormat
        capturedImagePipelineStateDescriptor.stencilAttachmentPixelFormat = view.depthStencilPixelFormat
        do {
            return try device.makeRenderPipelineState(descriptor: capturedImagePipelineStateDescriptor)
        } catch {
            fatalError("Could not create render pipeline state object: \(error)")
        }
    }
}
