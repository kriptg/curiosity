//
//  CapturedImageRenderer.swift
//  curiosity v0.1
//
//  Created by Kiryl Mikhailovich on 5/6/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import Foundation
import MetalKit
import simd
import ARKit

class CapturedImageRenderer {

    var session: ARSession
    let device: MTLDevice

    let sceneSettingsRenderer: AbstractFactorySettingsRenderer
    let sceneSettings: SceneSettings?

    var pixelBuffer: CVPixelBuffer?
    let commandQueue: MTLCommandQueue?
    var capturedImageTextureCache: CVMetalTextureCache?
    var imagePlaneVertexBuffer: MTLBuffer?

    var capturedImageTextureY: MTLTexture?
    var capturedImageTextureCbCr: MTLTexture?

    var viewportSize: CGSize = CGSize()
    var viewportSizeDidChange: Bool = false

    let planeVertexData: [Float] = [-1, -1, 0, 1,
                                    1, -1, 1, 1,
                                    -1, 1, 0, 0,
                                    1, 1, 1, 0]

    init(device: MTLDevice, session: ARSession, view: MTKView, commandQueue: MTLCommandQueue?) {

        sceneSettingsRenderer = CaptureImageSettingsRenderer()
        sceneSettings = sceneSettingsRenderer.buildSettings(device: device, view: view)

        self.device = device
        self.session = session
        self.commandQueue = commandQueue

        let imagePlaneVertexDataCount = planeVertexData.count * MemoryLayout<Float>.size
        imagePlaneVertexBuffer = device.makeBuffer(bytes: planeVertexData, length: imagePlaneVertexDataCount, options: [])
    }

    func draw(in view: MTKView, commandEncoder: MTLRenderCommandEncoder) {

        var textureCache: CVMetalTextureCache?
        CVMetalTextureCacheCreate(nil, nil, device, nil, &textureCache)
        capturedImageTextureCache = textureCache

        pixelBuffer = session.currentFrame?.capturedImage
        updateGameState()

        drawCapturedImage(renderEncoder: commandEncoder)
    }

    func drawCapturedImage(renderEncoder: MTLRenderCommandEncoder) {
        guard capturedImageTextureY != nil && capturedImageTextureCbCr != nil else { return }

        renderEncoder.pushDebugGroup("DrawCapturedImage")
        renderEncoder.setCullMode(.none)

        guard let renderPipeline = sceneSettings?.renderPipeline else { return }
        renderEncoder.setRenderPipelineState(renderPipeline)

        guard let depthStencilState = sceneSettings?.depthStencilState else { return }
        renderEncoder.setDepthStencilState(depthStencilState)

        renderEncoder.setVertexBuffer(imagePlaneVertexBuffer, offset: 0, index: 0)
        renderEncoder.setFragmentTexture(capturedImageTextureY, index: 1)
        renderEncoder.setFragmentTexture(capturedImageTextureCbCr, index: 2)
        renderEncoder.drawPrimitives(type: .triangleStrip, vertexStart: 0, vertexCount: 4)
        renderEncoder.popDebugGroup()
    }

    func updateCapturedImageTextures() {

        pixelBuffer = session.currentFrame?.capturedImage

        guard let pixelBuffer = pixelBuffer else {
            return
        }

        if CVPixelBufferGetPlaneCount(pixelBuffer) < 2 {
            return
        }

        capturedImageTextureY = createTexture(fromPixelBuffer: pixelBuffer, pixelFormat: .r8Unorm, planeIndex: 0)
        capturedImageTextureCbCr = createTexture(fromPixelBuffer: pixelBuffer, pixelFormat: .rg8Unorm, planeIndex: 1)
    }

    func createTexture(fromPixelBuffer pixelBuffer: CVPixelBuffer, pixelFormat: MTLPixelFormat, planeIndex: Int) -> MTLTexture? {
        var mtlTexture: MTLTexture?
        let width = CVPixelBufferGetWidthOfPlane(pixelBuffer, planeIndex)
        let height = CVPixelBufferGetHeightOfPlane(pixelBuffer, planeIndex)

        var texture: CVMetalTexture?

        guard let capturedImageTextureCache = capturedImageTextureCache else { return nil }
        let status = CVMetalTextureCacheCreateTextureFromImage(nil,
                                                               capturedImageTextureCache,
                                                               pixelBuffer,
                                                               nil,
                                                               pixelFormat,
                                                               width,
                                                               height,
                                                               planeIndex,
                                                               &texture)
        if status == kCVReturnSuccess {
            guard let texture = texture else { return nil }
            mtlTexture = CVMetalTextureGetTexture(texture)
        }

        return mtlTexture
    }

    func updateImagePlane(frame: ARFrame) {
        let displayToCameraTransform = frame.displayTransform(for: .portrait, viewportSize: viewportSize).inverted()
        guard let vertexData = imagePlaneVertexBuffer?.contents().assumingMemoryBound(to: Float.self) else { return }
        for index in 0...3 {
            let textureCoordIndex = 4 * index + 2
            let textureCoord = CGPoint(x: CGFloat(planeVertexData[textureCoordIndex]), y: CGFloat(planeVertexData[textureCoordIndex + 1]))
            let transformedCoord = textureCoord.applying(displayToCameraTransform)
            vertexData[textureCoordIndex] = Float(transformedCoord.x)
            vertexData[textureCoordIndex + 1] = Float(transformedCoord.y)
        }
    }

    func updateGameState() {

        guard let currentFrame = session.currentFrame else {
            return
        }

        updateCapturedImageTextures()
        if viewportSizeDidChange {
            viewportSizeDidChange = false
            updateImagePlane(frame: currentFrame)
        }
    }
}
