//
//  Renderer.swift
//  curiosity v0.1
//
//  Created by Kiryl Mikhailovich on 4/4/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import Foundation
import MetalKit
import simd
import ARKit

class Renderer {

    var session: ARSession
    let device: MTLDevice
    var map: Map

    let commandQueue: MTLCommandQueue?

    let geometryRenderer: GeometryRenderer
    let capturedImageRenderer: CapturedImageRenderer

    init(session: ARSession, view: MTKView, device: MTLDevice, map: Map) {

        self.device = device
        self.map = map
        self.session = session
        commandQueue = device.makeCommandQueue()

        self.geometryRenderer = GeometryRenderer(device: device,
                                                 session: session,
                                                 view: view,
                                                 map: map,
                                                 commandQueue: commandQueue)

         self.capturedImageRenderer = CapturedImageRenderer(device: device,
                                                            session: session,
                                                            view: view,
                                                            commandQueue: commandQueue)
    }

    func draw(in view: MTKView) {

        guard let commandBuffer = commandQueue?.makeCommandBuffer() else { return }

        if let renderPassDescriptor = view.currentRenderPassDescriptor, let drawable = view.currentDrawable {

            guard let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor) else { return }

            capturedImageRenderer.draw(in: view, commandEncoder: commandEncoder)
            geometryRenderer.draw(in: view, commandEncoder: commandEncoder)

            commandEncoder.endEncoding()
            commandBuffer.present(drawable)
            commandBuffer.commit()
        }
    }

    func setDataToDrawWay(_ wayArray: [[Int]]?) {
        geometryRenderer.optimalWayArray = wayArray
    }

    func updateTexturesOnEdit() {
        geometryRenderer.updateTexturesOnEdit()
    }

    func drawRectResized(size: CGSize) {
        capturedImageRenderer.viewportSize = size
        capturedImageRenderer.viewportSizeDidChange = true
    }
}
