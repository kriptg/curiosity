//
//  GeometryRenderer.swift
//  curiosity v0.1
//
//  Created by Kiryl Mikhailovich on 5/6/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import Foundation
import MetalKit
import simd
import ARKit

struct VertexUniforms {
    var viewProjectionMatrix: float4x4
    var modelMatrix: float4x4
    var normalMatrix: float3x3
}

struct FragmentUniforms {
    var cameraWorldPosition = float3(0, 0, 0)
    var specularColor = float3(1, 1, 1)
    var specularPower = Float(1)
}

class GeometryRenderer {

    var session: ARSession
    let device: MTLDevice

    let sceneSettingsRenderer: AbstractFactorySettingsRenderer

    let sceneSettings: SceneSettings?
    let commandQueue: MTLCommandQueue?
    var scene: Scene?
    var optimalWayArray: [[Int]]?

    private var map: Map

    private var time: Float = 0
    private var viewAngle = Float.pi / 6
    private var step = 0
    private var roverRotationAngle = Float(0)
    private var rotatePermission = true
    private var roverPosition = float3(0, 0, 0)
    private var needRoverPosition = float3(0, 0, 0)
    private var needPositionWasChanged = false
    private var scaleForFloor = Float(1.0)
    private var floorScalePermission = true

    var cameraWorldPosition = float3(0, 0, 0)
    var viewMatrix = matrix_identity_float4x4
    var projectionMatrix = matrix_identity_float4x4

    var xCountOfCells = 9
    var yCountOfCells = 16

    enum Textures: Int {
        case stone = 1, hill, sand, hole

        var name: String {
            switch self {
            case .stone:
                return "stone"
            case .hill:
                return "hill"
            case .sand:
                return "sand"
            case .hole:
                return "hole"
            }
        }
    }

    init(device: MTLDevice, session: ARSession, view: MTKView, map: Map, commandQueue: MTLCommandQueue?) {

        self.map = map
        self.device = device
        self.session = session
        self.commandQueue = commandQueue

        sceneSettingsRenderer = GeometrySettingsRenderer()
        sceneSettings = sceneSettingsRenderer.buildSettings(device: device, view: view)

        guard let vertexDescriptor = sceneSettings?.vertexDescriptor else { return }
        scene = buildScene(device: device, vertexDescriptor: vertexDescriptor, map: map)
    }

    func buildScene(device: MTLDevice, vertexDescriptor: MDLVertexDescriptor, map: Map) -> Scene {
        let bufferAllocator = MTKMeshBufferAllocator(device: device)
        let textureLoader = MTKTextureLoader(device: device)
        let options: [MTKTextureLoader.Option: Any] = [.generateMipmaps: true, .SRGB: true]

        var scene = Scene()

        let roverMaterial = Material()
        roverMaterial.specularPower = 100
        roverMaterial.specularColor = float3(0.8, 0.8, 0.8)
        guard let roverURL = Bundle.main.url(forResource: "rover", withExtension: "obj") else { return scene }
        let roverBaseColorTexture = try? textureLoader.newTexture(name: "metal",
                                                                  scaleFactor: 1.0,
                                                                  bundle: nil,
                                                                  options: options)
        roverMaterial.baseColorTexture = roverBaseColorTexture

        let roverAsset = MDLAsset(url: roverURL, vertexDescriptor: vertexDescriptor, bufferAllocator: bufferAllocator)

        let rover = Node(name: "Rover")

        do {
            let roverMesh = try MTKMesh.newMeshes(asset: roverAsset, device: device).metalKitMeshes.first
            rover.mesh = roverMesh
        } catch { }

        rover.material = roverMaterial
        scene.rootNode.children.append(rover)

        let mapNode = Node(name: "Map")
        scene.rootNode.children.append(mapNode)

        scene = updateTexture(device: device, vertexDescriptor: vertexDescriptor, map: map, scene: scene)

        return scene
    }

    func updateTexture(device: MTLDevice, vertexDescriptor: MDLVertexDescriptor, map: Map, scene: Scene) -> Scene {

        let bufferAllocator = MTKMeshBufferAllocator(device: device)
        let textureLoader = MTKTextureLoader(device: device)
        let options: [MTKTextureLoader.Option: Any] = [.generateMipmaps: true, .SRGB: true]

        let xCountOfCells = 9
        let yCountOfCells = 16

        if let mapNode = scene.nodeNamed ("Map") {
            mapNode.children = []
            for i in 0...xCountOfCells - 1 {
                for j in 0...yCountOfCells - 1 {

                    let cellMaterial = Material()
                    cellMaterial.specularPower = 40
                    cellMaterial.specularColor = float3(0.8, 0.8, 0.8)
                    guard let cellURL = Bundle.main.url(forResource: "mount", withExtension: "obj") else { return scene }

                    guard let textureName = Textures(rawValue: map.typeMatrix[i][j])?.name else { return scene }

                    let cellBaseColorTexture = try? textureLoader.newTexture(name: textureName,
                                                                             scaleFactor: 1.0,
                                                                             bundle: nil,
                                                                             options: options)
                    cellMaterial.baseColorTexture = cellBaseColorTexture

                    let cellAsset = MDLAsset(url: cellURL, vertexDescriptor: vertexDescriptor, bufferAllocator: bufferAllocator)
                    let cell = Node(name: "Cell \(i)\(j)")

                    do {
                        let cellMesh = try MTKMesh.newMeshes(asset: cellAsset, device: device).metalKitMeshes.first
                        cell.mesh = cellMesh
                    } catch { }
                    cell.material = cellMaterial

                    mapNode.children.append(cell)
                }
            }
        }

        return scene
    }

    func draw(in view: MTKView, commandEncoder: MTLRenderCommandEncoder) {

        initScene(view)

        if optimalWayArray != nil {
            drawOptimalWayUpdate(view)
        } else {
            roverPosition = float3(-10 + scaleForFloor * 2 * 4, 0, scaleForFloor * 2 * 14)
            needRoverPosition = float3(-10 + scaleForFloor * 2 * 4, 0, scaleForFloor * 2 * 14)
            roverRotationAngle = Float(0)
            needPositionWasChanged = false
            step = 0
        }

        guard let frame = session.currentFrame else { return }
        if !frame.anchors.isEmpty {
            drawMap(renderEncoder: commandEncoder)
        }
    }

    func drawMap(renderEncoder: MTLRenderCommandEncoder) {
        renderEncoder.setFrontFacing(.counterClockwise)
        renderEncoder.setCullMode(.back)

        guard let depthStencilState = sceneSettings?.depthStencilState else { return }
        renderEncoder.setDepthStencilState( depthStencilState)

        guard let renderPipeline = sceneSettings?.renderPipeline else { return }
        renderEncoder.setRenderPipelineState(renderPipeline)

        guard let samplerState = sceneSettings?.samplerState else { return }
        renderEncoder.setFragmentSamplerState(samplerState, index: 0)

        guard let scene = scene else { return }
        drawNodeRecursive(scene.rootNode, parentTransform: matrix_identity_float4x4, commandEncoder: renderEncoder)
        renderEncoder.popDebugGroup()
    }

    func drawNodeRecursive(_ node: Node, parentTransform: float4x4, commandEncoder: MTLRenderCommandEncoder) {
        let modelMatrix = parentTransform * node.modelMatrix

        if let mesh = node.mesh, let baseColorTexture = node.material.baseColorTexture {
            let viewProjectionMatrix = projectionMatrix * viewMatrix
            var vertexUniforms = VertexUniforms(viewProjectionMatrix: viewProjectionMatrix,
                                                modelMatrix: modelMatrix,
                                                normalMatrix: modelMatrix.normalMatrix)
            commandEncoder.setVertexBytes(&vertexUniforms, length: MemoryLayout<VertexUniforms>.size, index: 1)

            var fragmentUniforms = FragmentUniforms(cameraWorldPosition: cameraWorldPosition,
                                                    specularColor: node.material.specularColor,
                                                    specularPower: node.material.specularPower)
            commandEncoder.setFragmentBytes(&fragmentUniforms, length: MemoryLayout<FragmentUniforms>.size, index: 0)

            commandEncoder.setFragmentTexture(baseColorTexture, index: 0)

            guard let vertexBuffer = mesh.vertexBuffers.first else { return }

            commandEncoder.setVertexBuffer(vertexBuffer.buffer, offset: vertexBuffer.offset, index: 0)

            for submesh in mesh.submeshes {
                let indexBuffer = submesh.indexBuffer
                commandEncoder.drawIndexedPrimitives(type: submesh.primitiveType,
                                                     indexCount: submesh.indexCount,
                                                     indexType: submesh.indexType,
                                                     indexBuffer: indexBuffer.buffer,
                                                     indexBufferOffset: indexBuffer.offset)
            }
        }

        for child in node.children {
            drawNodeRecursive(child, parentTransform: modelMatrix, commandEncoder: commandEncoder)
        }
    }

    func drawOptimalWayUpdate (_ view: MTKView) {
        var needRotationAngle = Float(0)
        let speed = Float(24) * scaleForFloor

        let startViewAngle = Float.pi / 2
        let changePositionDelta = Float(2) * scaleForFloor

        time = 0.1 / Float(view.preferredFramesPerSecond)

        if viewAngle > startViewAngle {
            viewAngle -= time
        }

        guard let optimalWayArray = optimalWayArray, step != optimalWayArray.count - 1  else { return }

        if optimalWayArray[step][0] == optimalWayArray[step + 1][0] {
            needRotationAngle = 0
            if !needPositionWasChanged {
                needRoverPosition.z -= changePositionDelta
                needPositionWasChanged = true
            }
        } else if optimalWayArray[step][0] - optimalWayArray[step + 1][0] == 1 {
            needRotationAngle = .pi / 2
            if !needPositionWasChanged {
                needRoverPosition.x -= changePositionDelta
                needPositionWasChanged = true
            }
        } else {
            needRotationAngle = -.pi / 2
            if !needPositionWasChanged {
                needRoverPosition.x += changePositionDelta
                needPositionWasChanged = true
            }
        }

        updatePosition(needRotationAngle, speed)

        if abs(roverPosition.z - needRoverPosition.z) < time * speed {
            if abs(roverPosition.x - needRoverPosition.x) < time * speed {
                step += 1
                rotatePermission = true
                needPositionWasChanged = false
            }
        }
    }

    func updatePosition(_ needRotationAngle: Float, _ speed: Float) {
        var speed = speed
        if rotatePermission {
            if abs(roverRotationAngle - needRotationAngle) < time * speed {
                rotatePermission = false
            } else if roverRotationAngle < needRotationAngle {
                roverRotationAngle += time * speed
            } else {
                roverRotationAngle -= time * speed
            }
        } else {
            if abs(needRoverPosition.z - roverPosition.z) > time * speed {
                speed = countSpeedForTexture(needPosition: needRoverPosition.z, actualPosition: roverPosition.z, step: step, speed: speed)
                roverPosition.z -= time * speed
                let speedByY = Float(3) * speed / 10
                cameraWorldPosition.y += time * speedByY
                cameraWorldPosition.z -= time * speed
            } else if needRoverPosition.x - roverPosition.x < 0 {
                speed = countSpeedForTexture(needPosition: needRoverPosition.x, actualPosition: roverPosition.x, step: step, speed: speed)
                roverPosition.x -= time * speed
                cameraWorldPosition.x -= time * speed
            } else {
                speed = countSpeedForTexture(needPosition: needRoverPosition.x, actualPosition: roverPosition.x, step: step, speed: speed)
                roverPosition.x += time * speed
                cameraWorldPosition.x += time * speed
            }
        }
    }

    func countSpeedForTexture(needPosition: Float, actualPosition: Float, step: Int, speed: Float) -> Float {

        guard let optimalWayArray = optimalWayArray else { return 0.0 }

        if abs(needPosition - actualPosition) > 1 {
            let xCoordinate = optimalWayArray[step][0]
            let yCoordinate = optimalWayArray[step][1]
            let textureCoefficient = map.typeMatrix[xCoordinate][yCoordinate]
            return speed / Float(textureCoefficient)
        } else {
            let xCoordinate = optimalWayArray[step + 1][0]
            let yCoordinate = optimalWayArray[step + 1][1]
            let textureCoefficient = map.typeMatrix[xCoordinate][yCoordinate]
            return speed / Float(textureCoefficient)
        }
    }

    func updateTexturesOnEdit() {
        guard let vertexDescriptor = sceneSettings?.vertexDescriptor else { return }
        guard let scene = scene else { return }
        self.scene = updateTexture(device: device, vertexDescriptor: vertexDescriptor, map: map, scene: scene)
    }

    func initScene(_ view: MTKView) {

        guard let frame = session.currentFrame else { return }

        if !frame.anchors.isEmpty {

            let anchor = frame.anchors[0]

            cameraWorldPosition.z = frame.camera.transform.columns.3.z * 100
            cameraWorldPosition.y = frame.camera.transform.columns.3.y * 100
            cameraWorldPosition.x = frame.camera.transform.columns.3.x * 100

            viewMatrix = frame.camera.viewMatrix(for: .portrait)
                        * float4x4(translationBy: -cameraWorldPosition)
                        * float4x4(rotationAbout: float3(0, 1, 0), by: .pi)

            let viewportSize = view.frame.size

            projectionMatrix = frame.camera.projectionMatrix(for: .portraitUpsideDown,
                                                             viewportSize: viewportSize,
                                                             zNear: 0.2,
                                                             zFar: CGFloat(Float.greatestFiniteMagnitude))

            var coordinateSpaceTransform = matrix_identity_float4x4
            coordinateSpaceTransform.columns.2.z = -1.0

            let modelMatrixSpace = anchor.transform
            let cellSizeCoefficient = Float(0.29)
            let scale = 1 / Float( xCountOfCells * yCountOfCells) * cellSizeCoefficient

            let anchorX = anchor.transform.columns.3.x * 100
            let anchorY = anchor.transform.columns.3.y * 100

            let minimalFloorDistance = Float(50)
            if floorScalePermission {
            scaleForFloor = cameraWorldPosition.y - anchorY > minimalFloorDistance ? Float(4) : Float(1)
            }

            floorScalePermission = false

            guard let scene = scene else { return }

            for i in 0...xCountOfCells - 1 {
                for j in 0...yCountOfCells - 1 {
                    if let cell = scene.nodeNamed("Cell \(i)\(j)") {
                        let scaleMatrix = float4x4(scaleBy: scale * scaleForFloor)
                        let translationCellVector = float3(-10 + anchorX + Float(i) * scaleForFloor * 2, anchorY, Float(j) * scaleForFloor * 2)
                        let translationMatrix = float4x4(translationBy: translationCellVector)
                        cell.modelMatrix = modelMatrixSpace * translationMatrix * scaleMatrix
                    }
                }
            }

            if let rover = scene.nodeNamed ("Rover") {

                let roverSizeCoefficient = Float(0.01 * scaleForFloor)
                let scaleMatrix = float4x4(scaleBy: roverSizeCoefficient)

                let roverpositionToDraw = float3 (roverPosition.x + anchorX,
                                                  roverPosition.y + anchorY,
                                                  roverPosition.z)

                let translationMatrix = float4x4(translationBy: roverpositionToDraw)
                let rotationMatrix = float4x4(rotationAbout: float3(0, 1, 0), by: roverRotationAngle)

                rover.modelMatrix = modelMatrixSpace * translationMatrix * scaleMatrix * rotationMatrix
            }
        } else {
            floorScalePermission = true
        }
    }
}
