//
//  AbstractFactoryRenderer.swift
//  curiosity v0.1
//
//  Created by Kiryl Mikhailovich on 5/3/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import Foundation
import MetalKit

protocol AbstractFactorySettingsRenderer {

    func buildVertexDescriptor() -> MDLVertexDescriptor?
    func buildSamplerState(device: MTLDevice) -> MTLSamplerState?
    func buildDepthStencilState(device: MTLDevice) -> MTLDepthStencilState?
    func buildPipeline(device: MTLDevice, view: MTKView, vertexDescriptor: MDLVertexDescriptor) -> MTLRenderPipelineState
    func buildSettings(device: MTLDevice, view: MTKView) -> SceneSettings?
}

extension AbstractFactorySettingsRenderer {

    func buildSettings(device: MTLDevice, view: MTKView) -> SceneSettings? {

        let sceneSettings = SceneSettings()

        sceneSettings.vertexDescriptor = buildVertexDescriptor()
        sceneSettings.samplerState = buildSamplerState(device: device)
        sceneSettings.depthStencilState = buildDepthStencilState(device: device)

        guard let vertexDescriptor = sceneSettings.vertexDescriptor else { return nil }
        sceneSettings.renderPipeline = buildPipeline(device: device, view: view, vertexDescriptor: vertexDescriptor)

        return sceneSettings
    }
}

class SceneSettings {

    var renderPipeline: MTLRenderPipelineState?
    var depthStencilState: MTLDepthStencilState?
    var vertexDescriptor: MDLVertexDescriptor?
    var samplerState: MTLSamplerState?
}
