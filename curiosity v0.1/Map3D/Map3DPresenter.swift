//
//  Map3DProtocol.swift
//  curiosity v0.1
//
//  Created by Kiryl Mikhailovich on 4/4/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import Foundation

protocol Map3DViewProtocol: class {
    func setNavigationBar(_ title: String)
    func setMap(_ map: Map)
    func passOptimalWayToRouter(_ way: [[Int]]?)
    func showEditView(_ mapPresenter: MapPresenter)
    func alertError(_ errorString: String, _ titleString: String)
    func startLoading()
    func stopLoading()
}

protocol Map3DPresenterProtocol {
    func attachView(_ view: Map3DViewProtocol)
    func detachView()
    func findOptimalWay()
    func showEditView()
}
class Map3DPresenter: Map3DPresenterProtocol {

    weak private var mapTableService: MapTableService?
    weak var map3DView: Map3DViewProtocol?

    private var startAnimationPermission = true
    private var map: Map

    init(mapService: MapTableService, map: Map) {
        self.mapTableService = mapService
        self.map = map
    }

    func attachView(_ view: Map3DViewProtocol) {
        view.setNavigationBar(map.mapName)
        view.setMap(map)
        view.startLoading()
        map3DView = view
    }

    func detachView() {
        map3DView = nil
    }

    func showEditView() {
        guard let mapTableService = mapTableService else { return }
        let mapPresenter = MapPresenter(mapService: mapTableService, map: map)
        self.map3DView?.showEditView(mapPresenter)
    }

    func findOptimalWay() {
        if startAnimationPermission {
            MapRouteClass(map: map).getOptimalWay { result in
                if result != nil {
                    guard let result = result else { return }
                    self.map3DView?.passOptimalWayToRouter(result)
                } else {
                    self.map3DView?.alertError("Can't find the way", "My battery is low and it is getting dark")
                }
                self.startAnimationPermission = false
            }
        } else {
           self.map3DView?.passOptimalWayToRouter(nil)
           startAnimationPermission = true
        }
    }
}
