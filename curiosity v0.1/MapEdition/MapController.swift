//
//  ViewController.swift
//  curiosity v0.1
//
//  Created by Kirill Mikhailovich on 2/26/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import UIKit

protocol MapControllerDelegate: class {
    func showController(map3DPresenter: Map3DPresenter)
}

class MapController: UIViewController, MapDetailsViewProtocol {

    internal var mapView: MapView?
    private let sandButton = UIButton(type: .custom)
    private let stoneButton = UIButton(type: .custom)
    private let holeButton = UIButton(type: .custom)
    private let hillButton = UIButton(type: .custom)

    private var lastTexture: UIButton?
    var mapPresenter: MapDetailsPresenterProtocol?
    weak var delegate: MapControllerDelegate?

    private var map: Map?

    override func viewDidLoad() {
        super.viewDidLoad()
        mapPresenter?.attachView(self)

        view.backgroundColor = .white
        let borderMap = CGFloat(5)

        let mapViewRect = CGRect(x: 0,
                                 y: 0,
                                 width: self.view.bounds.width,
                                 height: self.view.bounds.height)

        let insets = UIEdgeInsets(top: borderMap,
                                  left: borderMap,
                                  bottom: borderMap,
                                  right: borderMap)

        guard let map = map else { return }
        mapView = MapView(frame: mapViewRect.inset(by: insets), map: map)
        mapView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        guard let view = mapView else { return }
        self.view.addSubview(view)
        setToolBar()
    }

    override func viewDidDisappear(_ animated: Bool) {
        mapPresenter?.detachView()
    }

    func setMap(_ map: Map) {
        self.map = map
    }

    func alertError(_ errorString: String, _ titleString: String) {
        let alert = UIAlertController(title: "\(errorString)", message: "\(titleString)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func setNavigationBar(_ title: String) {
        self.navigationItem.title = title
        let randomButton = UIBarButtonItem(title: "Random",
                                           style: .plain,
                                           target: self,
                                           action: #selector(randomCells))
        let saveButton = UIBarButtonItem(title: "Save",
                                         style: .plain,
                                         target: self,
                                         action: #selector(saveMap))
          self.navigationItem.rightBarButtonItems = [saveButton, randomButton]
    }

    @objc func randomCells() {
        mapPresenter?.randomCells()
    }

    @objc func saveMap() {
        guard let presenter = mapPresenter else { return }
        presenter.updateMap()
    }

    private func setToolBar() {
        navigationController?.isToolbarHidden = false

        stoneButton.setBackgroundImage(UIImage(named: "stone.jpg" ), for: .normal)
        stoneButton.setBackgroundImage(UIImage(named: "stoneChoosed.jpg" ), for: .selected)
        stoneButton.isSelected = true
        lastTexture = stoneButton
        stoneButton.addTarget(self, action: #selector(chooseTexture(_:)), for: .touchDown)

        hillButton.setBackgroundImage(UIImage(named: "hill.jpg" ), for: .normal)
        hillButton.setBackgroundImage(UIImage(named: "hillChoosed.jpg" ), for: .selected)
        hillButton.addTarget(self, action: #selector(chooseTexture(_:)), for: .touchDown)

        sandButton.setBackgroundImage(UIImage(named: "sand.jpg"), for: .normal)
        sandButton.setBackgroundImage(UIImage(named: "sandChoosed.jpg"), for: .selected)
        sandButton.addTarget(self, action: #selector(chooseTexture(_:)), for: .touchDown)

        holeButton.setBackgroundImage(UIImage(named: "hole.jpg"), for: .normal)
        holeButton.setBackgroundImage(UIImage(named: "holeChoosed.jpg"), for: .selected)
        holeButton.addTarget(self, action: #selector(chooseTexture(_:)), for: .touchDown)

        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)

        toolbarItems = [UIBarButtonItem(customView: stoneButton),
                        spacer, UIBarButtonItem(customView: hillButton),
                        spacer, UIBarButtonItem(customView: sandButton),
                        spacer, UIBarButtonItem(customView: holeButton)]
    }

    @objc func chooseTexture(_ sender: UIButton?) {

        if lastTexture == sender { return }

        sender?.isSelected = true
        lastTexture?.isSelected = false
        lastTexture = sender

        switch sender {
        case sandButton:
            mapView?.choosedTexture = MapView.Textures.sand
        case stoneButton:
            mapView?.choosedTexture = MapView.Textures.stone
        case holeButton:
            mapView?.choosedTexture = MapView.Textures.hole
        case hillButton:
            mapView?.choosedTexture = MapView.Textures.hill
        default:
            return
        }
    }

    func drawOptimalWay(_ way: [[Int]]) {
        mapView?.drawOptimalWay(way)
    }

    func closeController(map3DPresenter: Map3DPresenter) {
        self.navigationController?.popViewController(animated: true)
        delegate?.showController(map3DPresenter: map3DPresenter)
    }
}
