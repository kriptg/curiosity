//
//  MapView.swift
//  curiosity v0.1
//
//  Created by Kirill Mikhailovich on 2/26/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import Foundation
import UIKit

class MapView: UIView {

    private var xCellSize: Double = 0
    private var yCellSize: Double = 0

    private var xCountOfCells: Int = 9
    private var yCountOfCells: Int = 16

    private var lastRedrawCell: CGPoint = CGPoint(x: -10, y: -10)

    private var map: Map
    private var optimalWay: [[Int]]?

    enum Textures: Int {
        case stone = 1, hill, sand, hole

        var path: String {
            switch self {
            case .stone:
                return "stone"
            case .hill:
                return "hill"
            case .sand:
                return "sand"
            case .hole:
                return "hole"
            }
        }

        var image: UIImage? {
            let image = UIImage(named: self.path)
            return image
        }

        var choosedImage: UIImage? {
            let image = UIImage(named: self.path + "Choosed")
            return image
        }
    }

    var choosedTexture = Textures.stone

    init(frame: CGRect, map: Map) {
        self.map = map
        super.init (frame: frame)
        self.backgroundColor = .white
    }

    @available(*, unavailable)
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func redrawCurrentCell(point: CGPoint) {

        let xPosition = Int(Double(point.x) / xCellSize)
        let yPosition = Int(Double(point.y) / yCellSize)

        let xCoordinate = Double(xPosition) * xCellSize
        let yCoordinate = Double(yPosition) * yCellSize

        if  xPosition >= 0 && xPosition <= 8 {
            if yPosition >= 0 && yPosition <= 15 {
                if Double(lastRedrawCell.x) != xCoordinate || Double(lastRedrawCell.y) != yCoordinate {
                    map.typeMatrix[xPosition][yPosition] = choosedTexture.rawValue
                    setNeedsDisplay(CGRect(x: xCoordinate, y: yCoordinate, width: xCellSize, height: yCellSize))
                    lastRedrawCell = CGPoint(x: xCoordinate, y: yCoordinate)
                }
            }
        }
    }

    func drawOptimalWay(_ way: [[Int]]) {
        optimalWay = way
        setNeedsDisplay()
    }

    func clearOptimalWay() {
        optimalWay = nil
        setNeedsDisplay()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        clearOptimalWay()
        if let touch = touches.first {
            redrawCurrentCell(point: touch.location(in: self))
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        clearOptimalWay()
        if let touch = touches.first {
            redrawCurrentCell(point: touch.location(in: self))
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        clearOptimalWay()
        if let touch = touches.first {
            redrawCurrentCell(point: touch.location(in: self))
        }
    }

    override func draw (_ rect: CGRect) {

        guard let context = UIGraphicsGetCurrentContext() else { return }

        xCellSize = Double(rect.width) / Double(xCountOfCells)
        yCellSize = Double(rect.height) / Double(yCountOfCells)

        for xPosition in 0...xCountOfCells - 1 {
            for yPosition in 0...yCountOfCells - 1 {

                let xCoordinate = Double(xPosition) * xCellSize
                let yCoordinate = Double(yPosition) * yCellSize

                let drect = CGRect(x: xCoordinate, y: yCoordinate, width: xCellSize, height: yCellSize)

                var imageTexture = Textures(rawValue: map.typeMatrix[xPosition][yPosition])?.image

                if optimalWay != nil {
                    guard let optimalWay = optimalWay else { return }
                    if optimalWay.firstIndex(of: [xPosition, yPosition]) != nil {
                        imageTexture = Textures(rawValue: map.typeMatrix[xPosition][yPosition])?.choosedImage
                    }
                }

                guard let image = imageTexture else { return }
                guard let cgImage = image.cgImage else { return }
                context.draw(cgImage, in: drect)
            }
        }
    }
}
