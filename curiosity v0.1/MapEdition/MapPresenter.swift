//
//  ViewPresenter.swift
//  curiosity v0.1
//
//  Created by Kiryl Mikhailovich on 3/6/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import Foundation

protocol MapDetailsViewProtocol: class {
    var mapView: MapView? { get set }
    func setNavigationBar(_ title: String)
    func randomCells()
    func alertError(_ errorString: String, _ titleString: String)
    func setMap(_ map: Map)
    func drawOptimalWay(_ way: [[Int]])
    func closeController(map3DPresenter: Map3DPresenter)
}

protocol MapDetailsPresenterProtocol {
    func attachView(_ view: MapDetailsViewProtocol)
    func updateMap()
    func detachView()
    func randomCells()
    func findOptimalWay()
}
class MapPresenter: MapDetailsPresenterProtocol {

    weak private var mapService: MapTableService?
    weak private var mapDetailsView: MapDetailsViewProtocol?

    private var map: Map

    init(mapService: MapTableService, map: Map) {
        self.mapService = mapService
        self.map = map
    }

    func attachView(_ view: MapDetailsViewProtocol) {
        view.setNavigationBar(map.mapName)
        view.setMap(map)
        mapDetailsView = view
    }

    func updateMap() {
        guard let mapService = mapService else { return }
        mapService.updateMap(map.mapName, map.typeMatrix) { _ in
            self.mapDetailsView?.alertError("Can't update data", "Check internet connection and try again")
        }
        let map3DPresenter = Map3DPresenter(mapService: mapService, map: map)
        mapDetailsView?.closeController(map3DPresenter: map3DPresenter)
    }

    func findOptimalWay() {
        let mapRouteClass = MapRouteClass(map: map)
        mapRouteClass.getOptimalWay { result in
            if result != nil {
                guard let result = result else { return }
                self.mapDetailsView?.drawOptimalWay(result)
            } else {
                self.mapDetailsView?.alertError("Can't find way", "My battery is low and it is getting dark")
            }
        }
    }

    func detachView() {
        mapDetailsView = nil
    }

    func randomCells() {
        map.randomCells()
        mapDetailsView?.mapView?.clearOptimalWay()
    }
}
