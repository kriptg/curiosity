//
//  MapClass.swift
//  curiosity v0.1
//
//  Created by Kiryl Mikhailovich on 3/5/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import Foundation

class Map {
    public var mapName: String
    public var typeMatrix: [[Int]]

    private var xCountOfCells: Int = 9
    private var yCountOfCells: Int = 16

    init(name: String) {
        mapName = name
        self.typeMatrix = Array(repeating: Array(repeating: 1, count: yCountOfCells), count: xCountOfCells)
        randomCells()
    }

    init(name: String, array: [[Int]]) {
        mapName = name
        typeMatrix = array
    }

    public func randomCells() {
        for xPosition in 0...xCountOfCells - 1 {
            for yPosition in 0...yCountOfCells - 1 {
                typeMatrix[xPosition][yPosition] = Int.random(in: 0...4)
                if typeMatrix[xPosition][yPosition] == 0 {
                    typeMatrix[xPosition][yPosition] = 1
                }
            }
        }
    }
}
