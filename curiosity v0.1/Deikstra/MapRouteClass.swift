//
//  OptimalWay.swift
//  curiosity v0.1
//
//  Created by Kiryl Mikhailovich on 3/19/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import Foundation

class MapRouteClass {

    private var map: Map

    private var xCountOfCells: Int = 9
    private var yCountOfCells: Int = 16
    private var vertexCount: Int = 0
    private var startVertex = 64
    private var finishVertex = 79

    private var optimalWay: [[Int]]?

    enum Textures: Int {
        case stone = 1, hill, sand, hole
    }

    init(map: Map) {
        self.map = map
        findOptimalWay()
    }

    func createVertexArray() -> [[Int]] {
        var vertexArray: [[Int]] = []

        for xPosition in 0...xCountOfCells - 1 {
            for yPosition in 0...yCountOfCells - 1 {
                let type = map.typeMatrix[xPosition][yPosition]
                vertexArray.append([xPosition, yPosition, type])
            }
        }
        return vertexArray
    }

    func createGraphArray() -> [[Int]] {

        var vertexArray = createVertexArray()
        var graphArray: [[Int]] = []

        vertexCount = xCountOfCells * yCountOfCells

        for index in 0...vertexCount {

            var array: [Int] = []

            if index - 1 >= 0 && index % yCountOfCells != 0 {
                if vertexArray[index - 1][2] != Textures.hole.rawValue {
                    array.append(index - 1)
                }
            }

            if index + 1 < vertexCount && index % yCountOfCells != (yCountOfCells - 1) {
                if vertexArray[index + 1][2] != Textures.hole.rawValue {
                    array.append(index + 1)
                }
            }

            if index - yCountOfCells >= 0 {
                if vertexArray[index - yCountOfCells][2] != Textures.hole.rawValue {
                    array.append(index - yCountOfCells)
                }
            }

            if index + yCountOfCells < vertexCount {
                if vertexArray[index + yCountOfCells][2] != Textures.hole.rawValue {
                    array.append(index + yCountOfCells)
                }
            }

            graphArray.append(array)
        }
        return graphArray
    }

    func findOptimalWay() {

        var vertexArray = createVertexArray()
        var graphArray = createGraphArray()

        let upperBoundNumber = 10000
        var arrayWayLength = Array(repeating: upperBoundNumber, count: vertexCount)
        arrayWayLength[startVertex] = 0

        var markArray = Array(repeating: false, count: vertexCount)

        var previousVertexArray: [Int] = Array(repeating: 0, count: vertexCount)

        for _ in 0...vertexCount - 1 {
            var vertex = -1
            for j in 0...vertexCount - 1 {
                if !markArray[j] && ( vertex == -1 || arrayWayLength[j] < arrayWayLength[vertex]) {
                    vertex = j
                }
            }
            if arrayWayLength[vertex] == upperBoundNumber {
                break
            }
            markArray[vertex] = true

            var size = 0

            if !graphArray[vertex].isEmpty {
                size = graphArray[vertex].count - 1
                for j in 0...size {
                    let toVertex = graphArray[vertex][j]
                    let price = vertexArray[toVertex][2]

                    if arrayWayLength[vertex] + price < arrayWayLength [toVertex] {
                        arrayWayLength[toVertex] = arrayWayLength[vertex] + price
                        previousVertexArray[toVertex] = vertex
                    }
                }
            }
        }

        var wayArray: [[Int]] = []

        if arrayWayLength[finishVertex] != upperBoundNumber {
            var vertix = finishVertex
            while vertix != startVertex {
                wayArray.append([vertexArray[vertix][0], vertexArray[vertix][1]])
                vertix = previousVertexArray[vertix]
            }
            wayArray.append([vertexArray[startVertex][0], vertexArray[startVertex][1]])

            if vertexArray[startVertex][2] != Textures.hole.rawValue {
                optimalWay = wayArray
            }
        }
    }

    func getOptimalWay(result: @escaping ([[Int]]?) -> Void) {
        result(optimalWay)
    }
}
