//
//  MapService.swift
//  curiosity v0.1
//
//  Created by Kiryl Mikhailovich on 3/5/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import Foundation
import Firebase

class MapTableService {

    lazy var database = Firestore.firestore()

    init() {
        FirebaseApp.configure()
    }
    func setMaps(_ map: Map, errorHandler: ((Error) -> Void)?) {

        var dataarray: [String: [Int]] = [:]
        for i in 0...8 {
            dataarray["\(i)"] = map.typeMatrix[i]
        }

        database.collection("maps").document().setData ([
            "array": dataarray,
            "name": "\(map.mapName)"
        ]) { error in
            if error != nil {
                guard let error = error else { return }
                errorHandler?(error)
            }
        }
    }

    func deleteMap(_ name: String, errorHandler: ((Error) -> Void)?) {

    database.collection("maps")
        .whereField("name", isEqualTo: name)
        .getDocuments { querySnapshot, error in
                if error != nil {
                    guard let error = error else { return }
                    errorHandler?(error)
                } else {
                    let document = querySnapshot?.documents.first
                    document?.reference.delete()
                }
        }
    }

    func updateMap(_ previousName: String, _ newName: String, errorHandler: ((Error) -> Void)?) {

        database.collection("maps")
            .whereField("name", isEqualTo: previousName)
            .getDocuments { querySnapshot, error in
                if error != nil {
                    guard let error = error else { return }
                    errorHandler?(error)
                } else {
                    let document = querySnapshot?.documents.first
                    document?.reference.updateData([
                        "name": newName
                        ])
                }
            }
    }

    func updateMap(_ name: String, _ typeMatrix: [[Int]], errorHandler: ((Error) -> Void)?) {

        var dataarray: [String: [Int]] = [:]
        for i in 0...8 {
            dataarray["\(i)"] = typeMatrix[i]
        }

        database.collection("maps")
            .whereField("name", isEqualTo: name)
            .getDocuments { querySnapshot, error in
            if error != nil {
                guard let error = error else { return }
                errorHandler?(error)
            } else {
                let document = querySnapshot?.documents.first
                document?.reference.updateData([
                    "array": dataarray
                    ])
            }
            }
    }

    func refreshData(resultHandler: (([Map]) -> Void)? = nil,
                     errorHandler: ((Error) -> Void)? = nil) {

        database.collection("maps").getDocuments { maps, error in
            guard let maps = maps else {
                guard let errorOptional = error else { return }
                errorHandler?(errorOptional)
                return
            }
            var dataArray: [Map] = []
            for document in maps.documents {
                let map = Map(name: "Mars")
                map.mapName = document["name"] as? String ?? ""
                for i in 0...8 {
                    guard var array = document["array"] as? [String: [Int]] else { return }
                    guard let row = array["\(i)"] else { return }
                    map.typeMatrix[i] = row
                }
                dataArray.append(map)
            }
            resultHandler?(dataArray)
        }
    }
}
