//
//  MapsViewController.swift
//  curiosity v0.1
//
//  Created by Kiryl Mikhailovich on 3/4/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import Foundation
import UIKit

class MapTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MapsViewProtocol, MapControllerDelegate {

    private var tableView: UITableView?
    private var maps: [Map] = []
    var mapTablePresenter: MapsPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white

        tableView = UITableView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: self.view.bounds.width,
                                              height: self.view.bounds.height))
        tableView?.dataSource = self
        tableView?.delegate = self

        mapTablePresenter?.attachView(self)

        self.view = tableView
        setNavigationBar()
    }

    override func viewWillAppear(_ animated: Bool) {
        mapTablePresenter?.refreshData(result: nil)
    }

    func updateData(_ maps: [Map]) {
        self.maps = maps
    }

    func setNavigationBar() {

        let addButton = UIBarButtonItem(barButtonSystemItem: .add,
                                        target: self,
                                        action: #selector(addItem))
        let synchronizeButton = UIBarButtonItem(barButtonSystemItem: .refresh,
                                                target: self,
                                                action: #selector(refreshItem))
        navigationItem.rightBarButtonItem = addButton
        navigationItem.leftBarButtonItem = synchronizeButton
    }

    @objc func refreshItem() {
        mapTablePresenter?.refreshData(result: nil)
    }

    private func showInputField(mapName: String,
                                cancelHandler: ((UIAlertAction) -> Void)? = nil,
                                actionHandler: ((_ text: String?) -> Void)? = nil) {

        let alert = UIAlertController(title: "Map name", message: "", preferredStyle: .alert)
        alert.addTextField { (textField: UITextField) in
            textField.placeholder = mapName
        }

        alert.addAction(UIAlertAction(title: "Add", style: .destructive) { (_: UIAlertAction) in
                                      guard let textField = alert.textFields?.first else {
                                            actionHandler?(nil)
                                            return
                                        }
                                      actionHandler?(textField.text)
        })

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: cancelHandler))

        self.present(alert, animated: true, completion: nil)
    }

    @objc func addItem() {
        showInputField(mapName: "") {(input: String?) in
            guard let name = input else { return }
            guard let presenter = self.mapTablePresenter else { return }
            presenter.addItem(name)
        }
    }

    func refreshMaps() {
        tableView?.reloadData()
    }

    func showMapView(_ mapPresenter: Map3DPresenter) {
        let map3DController = Map3DController()
        map3DController.mapPresenter = mapPresenter
        navigationController?.pushViewController(map3DController, animated: true)
    }

    func showMapEditView(_ mapPresenter: MapPresenter) {
        let mapController = MapController()
        mapController.delegate = self
        mapController.mapPresenter = mapPresenter
        navigationController?.pushViewController(mapController, animated: true)
    }
    func alertError(_ title: String, _ message: String) {
        let alert = UIAlertController(title: "\(title)", message: "\(message)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "mapCell")
        cell.textLabel?.text = mapTablePresenter?.label(forRow: indexPath.row)
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return maps.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let presenter = mapTablePresenter else { return }
        presenter.didSelect(row: indexPath.row)
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let map = maps[indexPath.row]
        let name = map.mapName
        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { _, indexPath in
            self.showInputField(mapName: name) {(input: String?) in
                guard let presenter = self.mapTablePresenter else { return }
                guard let text = input else { return }
                presenter.editItem(name: text, forRow: indexPath.row)
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
        }

        let deleteAction = UITableViewRowAction(style: .default, title: "Delete") {  _, indexPath in
            self.mapTablePresenter?.deleteItem(forRow: indexPath.row)
             tableView.deleteRows(at: [indexPath], with: .fade)
        }

        return [deleteAction, editAction]
    }

    func showController(map3DPresenter: Map3DPresenter) {
        let map3DController = Map3DController()
        map3DController.mapPresenter = map3DPresenter
        navigationController?.pushViewController(map3DController, animated: true)
    }
}
