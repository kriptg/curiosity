//
//  MapPresenter.swift
//  curiosity v0.1
//
//  Created by Kiryl Mikhailovich on 3/5/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import Foundation

protocol MapsViewProtocol: class {
    func refreshMaps()
    func alertError(_ title: String, _ message: String)
    func showMapView(_ mapPresenter: Map3DPresenter)
    func showMapEditView(_ mapPresenter: MapPresenter)
    func updateData(_ maps: [Map])
}

protocol MapsPresenterProtocol: class {
    func attachView(_ view: MapsViewProtocol)
    func refreshData(result: (([Map]) -> Void)?)
    func addItem(_ name: String)
    func deleteItem(forRow row: Int)
    func editItem(name: String, forRow row: Int)
    func label(forRow row: Int) -> String
    func didSelect(row: Int)
}

class MapTablePresenter: MapsPresenterProtocol {

    weak private var mapsView: MapsViewProtocol?
    private var maps: [Map] = []

    let mapTableService: MapTableService

    init(mapTableService: MapTableService) {
        self.mapTableService = mapTableService
        refreshData(result: nil)
    }

    func attachView(_ view: MapsViewProtocol) {
        mapsView = view
        mapsView?.updateData(maps)
    }

    func refreshData(result: (([Map]) -> Void)?) {
        mapTableService.refreshData(
            resultHandler: { [unowned self] mapsArray in
                self.maps = mapsArray
                self.mapsView?.updateData(self.maps)
                self.mapsView?.refreshMaps()
                result?(self.maps)
            },
            errorHandler: { _ in
                self.mapsView?.alertError("Can't download maps", "Check inernet connection and try again")
            })
    }

    func addItem(_ name: String) {
        if maps.firstIndex(where: { $0.mapName == name }) != nil {
            mapsView?.alertError("Problem with name", "This name is taken. Try again")
            } else {
                let map = Map(name: name)
                maps.append(map)
                mapsView?.updateData(maps)
                mapTableService.setMaps(map) { _ in
                         self.mapsView?.alertError("Can't add map", "Check inernet connection and try again")
                         return
                }
            let mapPresenter = MapPresenter(mapService: self.mapTableService, map: map)
            self.mapsView?.showMapEditView(mapPresenter)
                }
        }

    func deleteItem(forRow row: Int) {
        mapTableService.deleteMap(maps[row].mapName) { _ in
               self.mapsView?.alertError("Can't delete map", "Check inernet connection and try again")
        }
        maps.remove(at: row)
        mapsView?.updateData(maps)
    }

    func editItem(name: String, forRow row: Int) {
        if maps.firstIndex(where: { $0.mapName == name }) != nil {
            mapsView?.alertError("Can't edit name", "This name is taken. Try again")
        } else {
            mapTableService.updateMap(maps[row].mapName, name) { _ in
                    self.mapsView?.alertError("Can't upadate name", "Check inernet connection and try again")
            }
            maps[row].mapName = name
            mapsView?.updateData(maps)
        }
    }

    func label(forRow row: Int) -> String {
        let map = maps[row]
        return map.mapName
    }

    func didSelect(row: Int) {
        refreshData { _ in
            let map = self.maps[row]
            let map3DPresenter = Map3DPresenter(mapService: self.mapTableService, map: map)
            self.mapsView?.showMapView(map3DPresenter)
        }
    }
}
