//
//  AppDelegate.swift
//  curiosity v0.1
//
//  Created by Kirill Mikhailovich on 2/26/19.
//  Copyright © 2019 Kirill Mikhailovich. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)

        let mapTableViewController = MapTableViewController()
        let mapTableService = MapTableService()
        mapTableViewController.mapTablePresenter = MapTablePresenter(mapTableService: mapTableService)
        let navigationController = UINavigationController(rootViewController: mapTableViewController)

        navigationController.navigationBar.isTranslucent = false
        navigationController.toolbar.isTranslucent = false

        navigationController.navigationBar.barTintColor = UIColor(red: 1, green: 0.64, blue: 0.47, alpha: 1 )
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]

        navigationController.toolbar.barTintColor = UIColor(red: 1, green: 0.64, blue: 0.47, alpha: 1 )
        navigationController.toolbar.tintColor = UIColor.white

        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        return true
    }
}
