# Сuriosity AR APP

An application that simulates the movement of the rover on the surface of Mars. All this happens in the nearest flat space using ARKit and Metal.

### Description

The image of the rover model is located in the source sector. The optimal path is calculated, the path is drawn on the map and the rover animated in sectors. The animation ends when the rover reaches the end sector.

### Tech stack

On this project was used:

* Swift 
* MVP
* Metal
* ARKit
* Firebase


